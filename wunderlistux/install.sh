#! /bin/bash

clear
cp ~/wunderlistux/wunderlistux-bin.desktop ~/.local/share/applications
echo "Insira sua senha de root:"
sudo cp ~/wunderlistux/wunderlistux-bin.desktop /usr/share/applications
clear
echo "Um lancador para o Wunderlistux foi instalado no menu de aplicacoes"
echo "Digite [1] para iniciar wunderlistux ou qualquer tecla para sair"
read input
if [ "$input" -eq 1 ];
then 
	echo " Iniciando wunderlistux "	
	exec ~/wunderlistux/wunderlistux.appimage
else
	exit
fi
